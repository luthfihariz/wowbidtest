package com.luthfihariz.wowbidtest.common

import android.arch.lifecycle.LiveData
import org.mockito.ArgumentMatchers.eq

fun <T> LiveData<T>.getTestObserver() = TestObserver<T>().apply {
    observeForever(this)
}

fun <T : Any> safeEq(value: T): T = eq(value) ?: value