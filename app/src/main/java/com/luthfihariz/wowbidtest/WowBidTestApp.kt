package com.luthfihariz.wowbidtest

import android.app.Application
import com.facebook.stetho.Stetho
import com.luthfihariz.wowbidtest.di.*
import org.koin.android.ext.android.startKoin

class WowBidTestApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(networkModule, appModule, viewModelModule, repositoryModule, adapterModule))

        Stetho.initializeWithDefaults(this)
    }
}