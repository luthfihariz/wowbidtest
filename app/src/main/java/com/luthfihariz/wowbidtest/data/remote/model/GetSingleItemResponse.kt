package com.luthfihariz.wowbidtest.data.remote.model

import com.google.gson.annotations.SerializedName


data class GetSingleItemResponse(
        @SerializedName("Timestamp") val timestamp: String,
        @SerializedName("Ack") val ack: String,
        @SerializedName("Build") val build: String,
        @SerializedName("Version") val version: String,
        @SerializedName("Item") val itemDetail: ItemDetail
)

data class ItemDetail(
        @SerializedName("ItemID") val itemID: String,
        @SerializedName("GalleryURL") val galleryURL: String,
        @SerializedName("PictureURL") val pictureURL: List<String>,
        @SerializedName("ListingStatus") val listingStatus: String,
        @SerializedName("Title") val title: String,
        @SerializedName("ItemSpecifics") val itemSpecifics: ItemSpecifics?
)

data class ItemSpecifics(
        @SerializedName("NameValueList") val nameValueList: List<NameValue>?
)

data class NameValue(
        @SerializedName("Name") val name: String,
        @SerializedName("Value") val value: List<String>
)