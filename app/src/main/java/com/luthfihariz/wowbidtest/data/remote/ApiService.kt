package com.luthfihariz.wowbidtest.data.remote

import com.luthfihariz.wowbidtest.data.remote.model.FindItemsResponse
import com.luthfihariz.wowbidtest.data.remote.model.GetSingleItemResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/Shopping?callname=GetSingleItem&IncludeSelector=ItemSpecifics")
    fun getSingleItems(@Query("ItemID") itemId:String) : Single<GetSingleItemResponse>

    @GET("https://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&REST-PAYLOAD")
    fun findItemsByKeyword(@Query("paginationInput.entriesPerPage") entries: Int = 10,
                           @Query("keywords") drone: String): Single<FindItemsResponse>
}