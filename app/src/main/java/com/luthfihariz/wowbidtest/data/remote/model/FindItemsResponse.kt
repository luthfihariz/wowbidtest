package com.luthfihariz.wowbidtest.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class FindItemsResponse(
        @SerializedName("findItemsByKeywordsResponse") val findItemsByKeywordsResponse: List<FindItemsByKeywordsResponse>
)

data class FindItemsByKeywordsResponse(
        @SerializedName("ack") val ack: List<String>,
        @SerializedName("version") val version: List<String>,
        @SerializedName("timestamp") val timestamp: List<String>,
        @SerializedName("searchResult") val searchResult: List<SearchResult>,
        @SerializedName("paginationOutput") val paginationOutput: List<PaginationOutput>,
        @SerializedName("itemSearchURL") val itemSearchURL: List<String>
)

data class SearchResult(
        @SerializedName("@count") val count: String,
        @SerializedName("item") val item: List<Item>
)

@Parcelize
data class Item(
        @SerializedName("itemId") val itemId: List<String>,
        @SerializedName("title") val title: List<String>,
        @SerializedName("globalId") val globalId: List<String>,
        @SerializedName("galleryURL") val galleryURL: List<String>,
        @SerializedName("sellingStatus") val sellingStatus: List<SellingStatus>,
        @SerializedName("subtitle") val subtitle: List<String>?,
        @SerializedName("galleryPlusPictureURL") val galleryPlusPictureURL: List<String>?
) : Parcelable

@Parcelize
data class SellingStatus(
        @SerializedName("currentPrice") val currentPrice: List<CurrentPrice>
) : Parcelable

@Parcelize
data class CurrentPrice(
        @SerializedName("@currencyId") val currencyId: String,
        @SerializedName("__value__") val value: String
) : Parcelable

@Parcelize
data class PaginationOutput(
        @SerializedName("pageNumber") val pageNumber: List<String>,
        @SerializedName("entriesPerPage") val entriesPerPage: List<String>,
        @SerializedName("totalPages") val totalPages: List<String>,
        @SerializedName("totalEntries") val totalEntries: List<String>
) : Parcelable