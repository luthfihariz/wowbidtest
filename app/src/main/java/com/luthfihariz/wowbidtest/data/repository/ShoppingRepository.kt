package com.luthfihariz.wowbidtest.data.repository

import com.luthfihariz.wowbidtest.data.remote.ApiService
import com.luthfihariz.wowbidtest.data.remote.model.Item
import com.luthfihariz.wowbidtest.data.remote.model.ItemDetail
import io.reactivex.Single

class ShoppingRepository(private val apiService: ApiService) {

    fun findItemsByKeyword(query: String, entries: Int): Single<List<Item>> {
        return apiService.findItemsByKeyword(entries, query).map {
            it.findItemsByKeywordsResponse[0].searchResult[0].item
        }
    }

    fun getSingleItems(itemId: String): Single<ItemDetail> {
        return apiService.getSingleItems(itemId).map {
            it.itemDetail
        }
    }
}