package com.luthfihariz.wowbidtest.presentation.productdetail

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.luthfihariz.wowbidtest.common.extension.executeIo
import com.luthfihariz.wowbidtest.common.rx.BaseSchedulerProvider
import com.luthfihariz.wowbidtest.data.Resource
import com.luthfihariz.wowbidtest.data.remote.model.ItemDetail
import com.luthfihariz.wowbidtest.data.repository.ShoppingRepository
import io.reactivex.rxkotlin.subscribeBy

class ProductDetailViewModel(private val shoppingRepository: ShoppingRepository,
                             private val scheduler: BaseSchedulerProvider) : ViewModel() {

    val itemDetailResource = MutableLiveData<Resource<ItemDetail>>()

    fun getSingleItem(itemId: String) {
        itemDetailResource.postValue(Resource.loading())
        shoppingRepository.getSingleItems(itemId).executeIo(scheduler).subscribeBy(
                onError = {
                    itemDetailResource.postValue(Resource.error(it))
                },
                onSuccess = {
                    itemDetailResource.postValue(Resource.success(it))
                }
        )
    }
}