package com.luthfihariz.wowbidtest.presentation.home

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.luthfihariz.wowbidtest.R
import com.luthfihariz.wowbidtest.common.base.BaseActivity
import com.luthfihariz.wowbidtest.common.extension.gone
import com.luthfihariz.wowbidtest.common.extension.setOnClickListener
import com.luthfihariz.wowbidtest.common.extension.visible
import com.luthfihariz.wowbidtest.common.widget.GridSpacingItemDecoration
import com.luthfihariz.wowbidtest.data.Resource
import com.luthfihariz.wowbidtest.data.Status
import com.luthfihariz.wowbidtest.data.remote.model.Item
import com.luthfihariz.wowbidtest.data.remote.model.ItemDetail
import com.luthfihariz.wowbidtest.presentation.productdetail.ProductDetailActivity
import com.luthfihariz.wowbidtest.presentation.productdetail.ProductDetailActivity.Companion.ARG_ITEM
import com.luthfihariz.wowbidtest.presentation.streamview.StreamViewActivity
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.view.*
import org.jetbrains.anko.startActivity
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class HomeActivity : BaseActivity() {

    private val viewModel by viewModel<HomeViewModel>()
    private val adapter by inject<HomeProductsAdapter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setupRecyclerView()
        initListener()

        with(viewModel) {
            productsResource.observe(this@HomeActivity, Observer { observeProductState(it) })
            findProducts()
        }
    }

    private fun initListener() {

        arrayListOf<View>(ivBanner, clMarqueeBanner, btnLiveStream).setOnClickListener {
            startActivity<StreamViewActivity>()
        }

        nsvRoot.setOnScrollChangeListener { _: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            if (scrollY >= resources.getDimensionPixelSize(R.dimen.banner_height)) {
                clMarqueeBanner.tvMarquee.isSelected = true
                clMarqueeBanner.visible()
            } else {
                clMarqueeBanner.gone()
            }
        }
    }

    private fun setupRecyclerView() {
        rvProducts.layoutManager = GridLayoutManager(this, 2)
        rvProducts.addItemDecoration(GridSpacingItemDecoration(2, resources.getDimensionPixelSize(R.dimen.spacing_small), true))
        adapter.clickListener = {
            startActivity<ProductDetailActivity>(ARG_ITEM to it)
        }
        rvProducts.adapter = adapter
    }

    private fun observeProductState(resource: Resource<List<Item>>?) {
        resource?.let {
            when (resource.status) {
                Status.SUCCESS -> {
                    hideLoading()
                    displayProducts(it.data)
                }
                Status.ERROR -> {
                    hideLoading()
                    showError(getString(R.string.generic_error_message))
                }
                Status.LOADING -> {
                    showLoading()
                }
            }
        }
    }

    private fun showLoading() {
        pbLoading.visible()
    }

    private fun hideLoading() {
        pbLoading.gone()
    }

    private fun displayProducts(data: List<Item>?) {
        adapter.items = data ?: arrayListOf()
    }

    private fun showError(message: String) {

    }
}