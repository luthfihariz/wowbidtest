package com.luthfihariz.wowbidtest.presentation.productdetail

import android.arch.lifecycle.Observer
import android.os.Bundle
import com.luthfihariz.wowbidtest.R
import com.luthfihariz.wowbidtest.common.base.BaseActivity
import com.luthfihariz.wowbidtest.common.extension.loadImageUrl
import com.luthfihariz.wowbidtest.data.Resource
import com.luthfihariz.wowbidtest.data.Status
import com.luthfihariz.wowbidtest.data.remote.model.Item
import com.luthfihariz.wowbidtest.data.remote.model.ItemDetail
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.activity_product_detail.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class ProductDetailActivity : BaseActivity() {

    companion object {

        const val ARG_ITEM = "itemDetail"
    }

    private val viewModel by viewModel<ProductDetailViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val item = intent.getParcelableExtra<Item>(ARG_ITEM)
        bindData(item)
        with(viewModel) {
            itemDetailResource.observe(this@ProductDetailActivity, Observer { observeItemDetailState(it) })
            getSingleItem(item.itemId[0])
        }
    }

    private fun observeItemDetailState(resource: Resource<ItemDetail>?) {
        resource?.let {
            when (it.status) {
                Status.LOADING -> {
                }
                Status.ERROR -> {
                }
                Status.SUCCESS -> {
                    bindItemDetail(it.data)
                }
            }
        }

    }

    private fun bindItemDetail(itemDetail: ItemDetail?) {
        itemDetail?.let { item ->
            tvSpec.text = item.itemSpecifics?.nameValueList?.joinToString(separator = "\n") {
                "${it.name} : ${it.value.joinToString()}"
            }

            ivProductImage.loadImageUrl(item.pictureURL[0])
        }
    }

    private fun bindData(item: Item) {
        supportActionBar?.title = item.title[0]
        tvTitle.text = item.title[0]
        tvReference.text = item.itemId[0]

        item.sellingStatus[0].currentPrice[0].apply {
            tvPrice.text = "$currencyId $value"
        }


    }
}