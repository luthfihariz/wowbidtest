package com.luthfihariz.wowbidtest.presentation.home

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.luthfihariz.wowbidtest.common.extension.executeIo
import com.luthfihariz.wowbidtest.common.rx.BaseSchedulerProvider
import com.luthfihariz.wowbidtest.data.Resource
import com.luthfihariz.wowbidtest.data.remote.model.Item
import com.luthfihariz.wowbidtest.data.remote.model.ItemDetail
import com.luthfihariz.wowbidtest.data.repository.ShoppingRepository
import io.reactivex.rxkotlin.subscribeBy

class HomeViewModel(private val shoppingRepository: ShoppingRepository,
                    private val scheduler: BaseSchedulerProvider) : ViewModel() {

    var productsResource = MutableLiveData<Resource<List<Item>>>()

    fun findProducts() {
        productsResource.postValue(Resource.loading())
        shoppingRepository.findItemsByKeyword("drone", 20)
                .executeIo(scheduler)
                .subscribeBy(
                        onError = {
                            productsResource.postValue(Resource.error(it))
                        },

                        onSuccess = {
                            productsResource.postValue(Resource.success(it))
                        }
                )
    }

}