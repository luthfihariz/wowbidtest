package com.luthfihariz.wowbidtest.presentation.home

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.luthfihariz.wowbidtest.R
import com.luthfihariz.wowbidtest.common.extension.inflate
import com.luthfihariz.wowbidtest.common.extension.loadImageUrl
import com.luthfihariz.wowbidtest.data.remote.model.Item
import kotlinx.android.synthetic.main.item_products.view.*

class HomeProductsAdapter : RecyclerView.Adapter<HomeProductsAdapter.HomeProductsViewHolder>() {

    var items: List<Item> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var clickListener: (Item) -> Unit = {}

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HomeProductsViewHolder =
            HomeProductsViewHolder(p0.inflate(R.layout.item_products, false))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(p0: HomeProductsViewHolder, p1: Int) {
        p0.bind(items[p1])
    }

    inner class HomeProductsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Item) {
            val image = item.galleryPlusPictureURL?.let { it[0] } ?: item.galleryURL[0]
            itemView.ivThumb.loadImageUrl(image)
            itemView.tvTitle.text = item.title[0]
            itemView.setOnClickListener {
                clickListener(item)
            }
        }

    }
}