package com.luthfihariz.wowbidtest.presentation.streamview

import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.PlaybackPreparer
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.google.android.exoplayer2.util.Util
import com.luthfihariz.wowbidtest.R
import com.luthfihariz.wowbidtest.common.base.BaseActivity
import kotlinx.android.synthetic.main.activity_stream_view.*

class StreamViewActivity : BaseActivity(), Player.EventListener, PlaybackPreparer {

    private var player: ExoPlayer? = null

    companion object {

        const val SAMPLE_LIVE_STREAM = "https://live.cnnindonesia.com/livecnn/smil:cnntv.smil/playlist.m3u8"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stream_view)

        playerView.requestFocus()
    }

    public override fun onStart() {
        super.onStart()
        if (Util.SDK_INT > 23) {
            initPlayer()
            playerView?.onResume()

        }
    }

    public override fun onResume() {
        super.onResume()
        if (Util.SDK_INT <= 23 || player == null) {
            initPlayer()
            playerView?.onResume()
        }
    }

    public override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= 23) {
            playerView?.onPause()
            releasePlayer()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
            playerView?.onPause()
            releasePlayer()
        }
    }

    private fun releasePlayer() {
        player?.release()
        player = null
    }

    private fun buildDataSourceFactory(): DataSource.Factory {
        return DefaultDataSourceFactory(this, buildHttpDataSourceFactory())

    }

    private fun buildHttpDataSourceFactory(): HttpDataSource.Factory {
        val userAgent = Util.getUserAgent(this, application.packageName)
        return DefaultHttpDataSourceFactory(userAgent)
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        Log.d("stream", "player state changed")
    }

    override fun onPositionDiscontinuity(reason: Int) {
        Log.d("stream", "position dicontinuity")
    }


    override fun preparePlayback() {
        Log.d("stream", "prepare playback")
        initPlayer()
    }


    private fun initPlayer() {
        player = ExoPlayerFactory.newSimpleInstance(this)
        player?.playWhenReady = true
        player?.addListener(this)
        playerView.player = player


        val mediaSource = HlsMediaSource.Factory(buildDataSourceFactory())
                .createMediaSource(Uri.parse(SAMPLE_LIVE_STREAM))
        player?.prepare(mediaSource)
    }

}