package com.luthfihariz.wowbidtest.di

import com.luthfihariz.wowbidtest.presentation.home.HomeProductsAdapter
import org.koin.dsl.module.module

val adapterModule = module {

    single { HomeProductsAdapter() }
}