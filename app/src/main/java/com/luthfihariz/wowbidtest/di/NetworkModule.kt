package com.luthfihariz.wowbidtest.di

import android.util.Log
import com.google.gson.GsonBuilder
import com.luthfihariz.wowbidtest.BuildConfig
import com.luthfihariz.wowbidtest.data.remote.ApiService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


val networkModule = module {
    single("logging") {
        HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
            Log.d("ApiLog", message)
        }).setLevel(HttpLoggingInterceptor.Level.BODY) as Interceptor
    }

    single("param_interceptor") {
        Interceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()

            val url = with(originalHttpUrl.newBuilder()) {
                if (originalHttpUrl.host() == "open.api.ebay.com") {
                    addQueryParameter("appid", "LuthfiHa-WowbidTe-PRD-eea9be394-45c8767b")
                            .addQueryParameter("version", "967")
                            .addQueryParameter("responseencoding", "JSON")
                } else {
                    addQueryParameter("SECURITY-APPNAME", "LuthfiHa-WowbidTe-PRD-eea9be394-45c8767b")
                            .addQueryParameter("SERVICE-VERSION", "1.0.0")
                            .addQueryParameter("RESPONSE-DATA-FORMAT", "JSON")
                }
            }.build()

            val requestBuilder = original.newBuilder().url(url)
            val request = requestBuilder.build()
            chain.proceed(request)
        }
    }


    single {
        OkHttpClient.Builder()
                .addInterceptor(get("logging"))
                .addInterceptor(get("param_interceptor"))
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build()
    }

    single("production_api") {
        val gson = GsonBuilder().setLenient().create()


        Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(get() as OkHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ApiService::class.java)
    }
}