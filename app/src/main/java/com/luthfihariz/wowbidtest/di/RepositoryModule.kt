package com.luthfihariz.wowbidtest.di

import com.luthfihariz.wowbidtest.data.repository.ShoppingRepository
import org.koin.dsl.module.module

val repositoryModule = module {

    single { ShoppingRepository(get("production_api")) }

}