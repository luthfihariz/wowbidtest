package com.luthfihariz.wowbidtest.di

import com.luthfihariz.wowbidtest.common.rx.BaseSchedulerProvider
import com.luthfihariz.wowbidtest.common.rx.SchedulerProvider
import org.koin.dsl.module.module

val appModule = module {

    single { SchedulerProvider() as BaseSchedulerProvider } // rx testing purpose

}