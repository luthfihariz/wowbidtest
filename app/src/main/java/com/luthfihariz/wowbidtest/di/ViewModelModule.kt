package com.luthfihariz.wowbidtest.di

import com.luthfihariz.wowbidtest.presentation.home.HomeViewModel
import com.luthfihariz.wowbidtest.presentation.productdetail.ProductDetailViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val viewModelModule = module {

    viewModel { HomeViewModel(get(), get()) }
    viewModel { ProductDetailViewModel(get(), get()) }
}